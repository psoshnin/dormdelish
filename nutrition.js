//Polina Soshnin
//CS249 Project 2
//Nutritionix API 

/* This function gets the values entered by the user in the text box and radio buttons
Then, calls the function prepare_request_URL() by passing these values as arguments.
*/
function start_search(){   
    if (document.getElementsByName("box")[0].value == "") 
	    alert("You need to enter an artist name.");
    /*	This currently doesn't pertain to my website because 
	I am not using these radio buttons
	else{
	    var searchTerm = document.getElementsByName("box")[0].value;
		var searchType = "albums";
	    if (document.getElementsByName("choice")[1].checked)
		    searchType = "artists";
		prepare_request_URL(searchTerm, searchType);	
		}*/
}

// nutritionix web service URL (see project documentation)
//this is my guess of what the nutritionix thing would look like:
//this is where my app key and my app ID go

var URL= "http://devapi.nutritionix.com/v1/api/item/?query=taco&appId=8e48b434&appKey=d6b0429f9bf282a81451a3348525f523";

/* This function prepares the complete URL to submit the request. Depending on the choice of the
user, it will add the right parts, which one knows from the API documentation.
The callback function names are arbitrary, they just reflect what the function will do with the
response from the web service.
*/

//need to change this is it complies with the nutritionix request

function prepare_request_URL(term, type){
    var method="";
	var callback = "";
	if (type == "search"){
	    method = "&method=artist.gettopalbums"; //need to change this parameter to fit my API
	    callback = "&callback=display_albums"; //same with this one!!!
	}
	else {
		method = "&method=artist.getsimilar&autocorrect=1";
		callback = "&callback=display_artists";
	}
	var requestURL = URL+method+"&artist="+encodeURI(term)+callback;
	console.log(requestURL);
    send_request(requestURL);
}

/* This function is straight from the HF HTML5 book, Ch.6, with only a slight 
modification, since we are passing the URL as an argument."
*/
function send_request(requestURL) {
	var newScriptElement = document.createElement("script");
	newScriptElement.setAttribute("src", requestURL);
	newScriptElement.setAttribute("id", "jsonp");
	var oldScriptElement = document.getElementById("jsonp");
	var head = document.getElementsByTagName("head")[0];
	if (oldScriptElement == null) {
		head.appendChild(newScriptElement);
	}
	else {
		head.replaceChild(newScriptElement, oldScriptElement);
	}
}